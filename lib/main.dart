import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Row & Column',
      theme: ThemeData(

        primarySwatch: Colors.grey,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('My Row and Column'),
        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Row(mainAxisAlignment: MainAxisAlignment.center,
                children:[
                  const CircleAvatar(
                    radius: 60,
                    backgroundImage: AssetImage('image/kim1.jpg'),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Hello,',
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        ),
                        Text('PARAMAPORN',
                          style: TextStyle(
                            fontSize: 30,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
